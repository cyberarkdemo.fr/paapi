# PAAPI - A PrivateArk API based on PACLI

This software is *WIP*, use at your own risks.

## Prerequisites

- Windows Server
- Python >= 3.9
- EPV Account
- PACLI

## Installation

1. Install PACLI
2. Install latest python release
3. Download and unzip this project
4. Copy .env.example to .env and fill in the required fields
5. Download python dependencies (`pip install -r requirements.txt`)
6. Run main.py (`python main.py`)

## TODO

- [ ] Swagger
- [ ] Prevent session ID collisions (using an array of existing session ids)
- [ ] Secure inputs to avoid code injection in PACLI commands (using regex?)
- [ ] Tests
    - [ ] Multiple sessions
    - [ ] Heavy file
    - [ ] Use behind reverse proxy

## Licensing

Copyright © 2021 CyberArk Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
