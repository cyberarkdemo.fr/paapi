#!/usr/bin/env python
# encoding: utf-8

import json
import os
import sys
import logging
import tempfile
import io
from flask import Flask, send_file, request, session, jsonify
from werkzeug.utils import secure_filename
from subprocess import PIPE, run
from random import randint
from settings import Settings

app = Flask(__name__)
app.config['SECRET_KEY'] ='changeme'

################################
# PACLI FUNCTIONS
################################

def out(command, workdir=None):
    logger = logging.getLogger()
    logger.debug(command)
    
    result = run(command,
                 stdout=PIPE,
                 stderr=PIPE,
                 universal_newlines=True,
                 shell=True,
                 cwd=workdir
                )

    if result.returncode != 0:
        logger.debug(f"RETURN CODE: {result.returncode }")
        logger.debug(f"STDOUT: {result.stdout}")
        logger.debug(f"STDERR: {result.stderr}")
    
    return [result.stdout,result.stderr]


def pacli(command, parameters=None):
    settings = Settings()

    if parameters:
        return out('.\Pacli ' +  command + ' SESSIONID=' + str(session["sessionid"]) + ' ' + parameters, settings.pacli_dir)
    else:
        return out('.\Pacli ' +  command + ' SESSIONID=' + str(session["sessionid"]), settings.pacli_dir)


################################
# API HANDLERS
################################

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


def check_authenticated():
    if "username" not in session:
        raise InvalidUsage('Unauthorized', 401)

################################
# API ROUTES
################################

@app.route('/')
def index():
    return 'PAAPI - A PrivateArk API'


@app.route('/login', methods = ['POST'])
def login():
    """ Create an API session and start a PACLI session """
    if "username" not in request.form or "password" not in request.form:
        raise InvalidUsage("Missing username or password", 401)
        
    session["username"] = request.form.get('username')
    session["sessionid"] = randint(0,255)
    
    settings = Settings()
    pacli('init')
    pacli('define', 'vault=' + settings.vault_name + ' address=' + settings.vault_ip)
    pacli('default', 'vault=' + settings.vault_name)
    pacli('logon', 'user=' + session["username"] + ' password=' + request.form.get('password'))

    return jsonify(dict(
        message = f"User {session['username']} authenticated"
        ))


@app.route('/logoff', methods = ['POST'])
def logoff():
    """ Logoff from PACLI and delete API session """
    check_authenticated()

    pacli('logoff', 'user=' + session["username"])
    pacli('term')

    username = session["username"]
    session.clear()

    return jsonify(dict(
        message = f"User {username} logged off"
        ))


@app.route('/user')
def get_session():
    """ Get user info """
    check_authenticated()

    return jsonify(dict(
        username = f"{session['username']}",
        session_id = f"{session['sessionid']}"
        ))


@app.route('/download', methods = ['POST'])
def download():
    """ Download file from Vault """
    check_authenticated()
    
    if not 'file' in request.form:
        raise InvalidUsage('Missing file parameter', 400)

    if not 'safe' in request.form:
        raise InvalidUsage('Missing safe parameter', 400)

    safe_name = request.form.get('safe')
    file_name = request.form.get('file')
    if safe_name == '' or file_name == '':
        raise InvalidUsage('File not found', status_code=404)

    with tempfile.TemporaryDirectory() as temp_dir:
        pacli('opensafe', 'user=' + session["username"] + ' safe=' + safe_name)
        pacli('retrievefile', 'user=' + session["username"] + ' safe=' + safe_name + ' folder=root file=' + file_name + ' localfolder=' + temp_dir + ' localfile=' + file_name)
        pacli('closesafe', 'user=' + session["username"] + ' safe=' + safe_name)

        file_path = os.path.join(temp_dir, file_name)
        if not os.path.exists(file_path):
            raise  InvalidUsage('File not found', status_code=404)

        # Load file in memory so it can be deleted from disk
        return_data = io.BytesIO()
        with open(file_path, 'rb') as fo:
            return_data.write(fo.read())
        # After writing, cursor will be at last byte, so move it to start
        return_data.seek(0)

    return send_file(return_data, download_name=file_name)


@app.route('/upload', methods = ['POST'])
def upload():
    """ Upload file to Vault """
    check_authenticated()

    if not 'file' in request.files:
        raise InvalidUsage('Missing file parameter', 400)

    if not 'safe' in request.form:
        raise InvalidUsage('Missing safe parameter', 400)

    file = request.files['file']
    safe_name = request.form.get('safe')
    file_name = secure_filename(file.filename)

    # Load the file in temporary folder until uploaded with pacli
    with tempfile.TemporaryDirectory() as temp_dir:
        file.save(os.path.join(temp_dir, file_name))
        pacli('opensafe', 'user=' + session["username"] + ' safe=' + safe_name)
        pacli('storefile', 'user=' + session["username"] + ' safe=' + safe_name + ' folder=root file=' + file_name + ' localfolder=' + temp_dir + ' localfile=' + file_name)
        pacli('closesafe', 'user=' + session["username"] + ' safe=' + safe_name)

    return jsonify(dict(
        message = f"Uploaded {file_name} to {safe_name}"
        ))

def main():
    print("---------------------------------")
    print("-------- STARTING PAAPI ---------")
    print("------- By cyberarklab.fr -------")
    print("---------------------------------")
    settings = Settings()

    # Logging configuration
    formatter = logging.Formatter('[%(asctime)s][%(name)s][%(levelname)s] -- %(message)s')

    logger = logging.getLogger()
    logger.setLevel(logging.getLevelName(settings.log_level))
    
    # File handler
    log_folder = settings.log_folder
    if log_folder:
        fh = logging.FileHandler('{}/paapi.log'.format(log_folder))
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    # Stdout handler
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    app.run()


if __name__ == "__main__":
    main()
