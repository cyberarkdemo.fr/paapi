""" Settings class file"""

import os

from dotenv import find_dotenv, load_dotenv

from singleton import Singleton


class Settings(metaclass=Singleton):
    """Read .env and give access to environment variables useful to the project."""
    def __init__(self):
        Settings.reload()

    @staticmethod
    def reload():
        """Reload .env"""
        load_dotenv(find_dotenv(), override=True)

    @property
    def log_folder(self):
        """Returns the log folder path"""
        return os.getenv("LOG_FOLDER", "")

    @property
    def log_level(self):
        """Returns the log level"""
        return os.getenv("LOG_LEVEL", "INFO")

    @property
    def vault_ip(self):
        """Returns the Vault IP"""
        return os.getenv("VAULT_IP")

    @property
    def vault_name(self):
        """Returns the Vault name"""
        return os.getenv("VAULT_NAME")

    @property
    def pacli_dir(self):
        """Returns the PACLI directory"""
        return os.getenv("PACLI_DIR")

    @property
    def api_secret(self):
        """Returns the API session secret"""
        return os.getenv("API_SECRET")
